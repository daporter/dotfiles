(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(org-agenda-files
   '("~/Dropbox/63_programming/63.02_leetcode/63.02_journal.org"))
 '(package-selected-packages
   '(dape modus-themes ef-themes kind-icon meow-tree-sitter popper hl-todo activities consult-denote csv-mode ledger-mode flymake-hledger hledger-mode 0blayout gptel vterm nov idlwave edit-indirect meow embrace apheleia auth-source-1password org-anki keyfreq expand-region ligature org-modern bind-key cl-generic cl-lib eglot eldoc erc external-completion faceup flymake jsonrpc let-alist map nadvice ntlm org python so-long soap-client svg tramp use-package verilog-mode xref page-break-lines pcmpl-args embark-consult embark avy nerd-icons-completion nerd-icons-ibuffer nerd-icons-dired pulsar which-key magit-todos rainbow-mode denote visual-fill-column sxhkdrc-mode editorconfig titlecase unfill org-noter pdf-tools olivetti lorem-ipsum flymake-markdownlint cape corfu yaml-mode marginalia orderless vertico adaptive-wrap magit python-mode smart-tabs-mode markdown-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
